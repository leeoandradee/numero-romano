package br.com.numero.romano;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class NumeroRomanoTeste {

    @Test
    public void testarConverteNumeroRomano() {
        String result = Conversor.conversorDeNumero(10);
        Assertions.assertEquals(result, "X");
    }

    @Test
    public void testarConverteNumeroRomano2() {
        String result = Conversor.conversorDeNumero(100);
        Assertions.assertEquals(result, "C");
    }


}
